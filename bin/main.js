#!/usr/bin/env node

import { program } from "commander";
import chalk from "chalk";
import "../lib/utils/mixin.mjs";
import { create } from "../lib/create.mjs";

program
  .version(
    `asl-cli ${global.loadJSON("../package.json", import.meta.url).version}`
  )
  .usage("<command> [options]");

program
  .command("create <app-name>")
  .description("创建项目")
  .action((name, options) => {
    console.log(chalk.bold.blue(`Asl Cli V1.0.0`));
    create(name, options);
  });

program.on("--help", () => {
  console.log();
  console.log(
    `Run ${chalk.yellow(
      `asl-cli <command> --help`
    )} for detailed usage of given command.`
  );
  console.log();
});

program.parse(process.argv);
