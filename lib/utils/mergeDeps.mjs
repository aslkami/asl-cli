export function mergeDeps(sourceDeps, depsToInject) {
  const result = Object.assign({}, sourceDeps);

  for (const depName in depsToInject) {
    const sourceRange = sourceDeps[depName];
    const injectingRange = depsToInject[depName];

    if (sourceRange === injectingRange) continue;

    result[depName] = injectingRange;
  }
  return result;
}

export const isObject = (val) => val && typeof val === "object";
