export * from "./sortObject.mjs";
export * from "./stringify.mjs";
export * from "./preset.mjs";
export * from "./prompt.mjs";
export * from "./writeFileTree.mjs";
export * from "./executeCommand.mjs";
export * from "./extractCallDir.mjs";
export * from "./mergeDeps.mjs";

export function printScripts(pkg) {
  const descriptions = {
    build: "Compiles and minifies for production",
    serve: "Compiles and hot-reloads for development",
    lint: "Lints and fixes files",
    "test:e2e": "Run your end-to-end tests",
    "test:unit": "Run your unit tests",
  };

  return Object.keys(pkg.scripts || {})
    .map((key) => {
      if (!descriptions[key]) return "";
      return [
        `\n### ${descriptions[key]}`,
        "```",
        `npm run ${key}`,
        "```",
        "",
      ].join("\n");
    })
    .join("");
}

export function generateReadme(pkg) {
  return [
    `# ${pkg.name}\n`,
    "## Project setup",
    "```",
    `npm install`,
    "```",
    printScripts(pkg),
    "### Customize configuration",
    "See [Configuration Reference](https://cli.vuejs.org/config/).",
    "",
  ].join("\n");
}
