import { stringify } from "javascript-stringify";

export function stringifyJS(value) {
  // 2个空格格式化显示
  return stringify(value, null, 2);
}
