import inquirer from "inquirer";
import chalk from "chalk";
import { execa } from "execa";
import cliUtils from "@vue/cli-shared-utils";
import { PromptModuleAPI } from "./PromptModuleAPI.mjs";
import PackageManager from "./PackageManager.mjs";
import Generator from "./Generator.mjs";
import {
  sortObject,
  defaults,
  vuePresets,
  getPromptModules,
  writeFileTree,
  generateReadme,
} from "./utils/index.mjs";

const { loadModule, hasGit, hasProjectGit } = cliUtils;

class Createor {
  constructor(name, ctx) {
    this.name = name;
    this.context = process.env.VUE_CLI_CONTEXT = ctx;
    this.pkg = {};
    this.pm = null;

    this.presetPrompt = this.resovePresetPrompts();
    this.featurePrompt = this.resolveFeaturePrompts();
    this.outroPrompts = this.resolveOutroPrompts();
    this.injectedPrompts = [];
    this.promptCompleteCbs = [];

    const promptAPI = new PromptModuleAPI(this);
    const promptModules = getPromptModules();
    promptModules.forEach((m) => m(promptAPI, chalk));
  }

  async create() {
    // 处理用户输入
    const presetRes = await this.promptAndResolvePreset();
    // 初始化安装环境
    await this.initPackageManagerEnv(presetRes);
    // 生成项目文件，生成配置文件
    const generator = await this.generate(presetRes);
    // 生成 readme
    await this.generateReadme(generator);

    this.finished();
  }

  // 选择 vue2 还是 vue 3 还是 手动选择 的 inquirer 询问配置
  resovePresetPrompts() {
    const presetChoices = Object.entries(defaults.presets).map(
      ([name, preset]) => {
        // "name": "Default (Vue 3)(@vue/cli-plugin-babel,@vue/cli-plugin-eslint)"
        // "value": "Default (Vue 3)"
        return {
          name: `${name}(${Object.keys(preset.plugins).join(",")})`, // 将预设的插件放到提示
          value: name,
        };
      }
    );

    return {
      name: "preset", // preset 记录用户选择的选项值。
      type: "list", // list 表单选
      message: `Please pick a preset:`,
      choices: [
        ...presetChoices, // Vue2 默认配置，Vue3 默认配置
        {
          name: "Manually select features", // 手动选择配置，自定义特性配置
          value: "__manual__",
        },
      ],
    };
  }

  // Babel、TypeScript、Vuex、PWA、Router、Vuex、CSS Pre-processors、Linter / Formatter、Unit Testing、E2E Testing
  resolveFeaturePrompts() {
    return {
      name: "features", // features 记录用户选择的选项值。
      when: (answers) => answers.preset === "__manual__", // 当选择"Manually select features"时，该提示显示
      type: "checkbox",
      message: "Check the features needed for your project:",
      choices: [], // 复选框值，待补充
      pageSize: 10,
    };
  }

  resolveOutroPrompts() {
    const outroPrompts = [
      // useConfigFiles 是单选框提示选项。
      {
        name: "useConfigFiles",
        when: (answers) => answers.preset === "__manual__",
        type: "list",
        message: "Where do you prefer placing config for Babel, ESLint, etc.?",
        choices: [
          {
            name: "In dedicated config files",
            value: "files",
          },
          {
            name: "In package.json",
            value: "pkg",
          },
        ],
      },
      // 确认提示选项
      {
        name: "save",
        when: (answers) => answers.preset === "__manual__",
        type: "confirm",
        message: "Save this as a preset for future projects?",
        default: false,
      },
      // 输入提示选项
      {
        name: "saveName",
        when: (answers) => answers.save,
        type: "input",
        message: "Save preset as:",
      },
    ];
    return outroPrompts;
  }

  resolveFinalPrompts() {
    const prompts = [
      this.presetPrompt,
      this.featurePrompt,
      ...this.outroPrompts,
      ...this.injectedPrompts,
    ];
    return prompts;
  }

  async promptAndResolvePreset() {
    try {
      let preset;
      const { name } = this;
      const answers = await inquirer.prompt(this.resolveFinalPrompts());

      // answers 得到的值为 { preset: 'Default (Vue 2)' }
      if (answers.preset && answers.preset === "Default (Vue 2)") {
        if (answers.preset in vuePresets) {
          preset = vuePresets[answers.preset];
        }
      } else {
        // 暂不支持 Vue3、自定义特性配置情况
        throw new Error("哎呀，出错了，暂不支持 Vue3、自定义特性配置情况");
      }

      // 添加 projectName 属性
      preset.plugins["@vue/cli-service"] = Object.assign(
        {
          projectName: name,
        },
        preset
      );

      return preset;
    } catch (err) {
      console.log(chalk.red(err));
      process.exit(1);
    }
  }

  async initPackageManagerEnv(preset) {
    const { name, context } = this;
    this.pm = new PackageManager({ context });

    console.log(`✨ 创建项目：${chalk.yellow(context)}`);

    // 用于生成 package.json 文件
    const pkg = {
      name,
      version: "0.1.0",
      private: true,
      devDependencies: {},
    };

    // 给 npm 包指定版本，简单做，使用最新的版本
    const deps = Object.keys(preset.plugins);
    // @vue/cli-plugin-babel, @vue/cli-plugin-babel, @vue/cli-service
    deps.forEach((dep) => {
      let { version } = preset.plugins[dep];
      if (!version) {
        version = "latest";
      }
      pkg.devDependencies[dep] = version;
    });

    this.pkg = pkg;

    // 写 package.json 文件
    await writeFileTree(context, {
      "package.json": JSON.stringify(pkg, null, 2),
    });

    // 初始化 git 仓库，以至于 vue-cli-service 可以设置 git hooks
    const shouldInitGit = this.shouldInitGit();
    if (shouldInitGit) {
      console.log(`🗃 初始化 Git 仓库...`);
      await this.run("git init");
    }

    // 安装插件 plugins
    console.log(`⚙ 正在安装 CLI plugins. 请稍候...`);

    await this.pm.install();
  }

  async generate(preset) {
    console.log(`🚀 准备相关文件...`);
    const { pkg, context } = this;

    const plugins = await this.resolvePlugins(preset.plugins, pkg);

    const generator = new Generator(context, {
      pkg,
      plugins,
    });

    // 赋值模板 start
    await generator.generate({
      extractConfigFiles: preset.useConfigFiles, // false
    });

    console.log(`🚀 相关文件已写入磁盘！`);

    await this.pm.install();

    return generator;
  }

  async generateReadme(generator) {
    console.log();
    console.log("📄 正在生成 README.md...");
    const { context } = this;
    await writeFileTree(context, {
      "README.md": generateReadme(generator.pkg),
    });
  }

  finished() {
    const { name } = this;
    console.log(`🎉 成功创建项目 ${chalk.yellow(name)}.`);
    console.log(
      `👉 用以下命令启动项目 :\n\n` +
        chalk.cyan(`cd ${name}\n`) +
        chalk.cyan(`npm run serve`)
    );
  }

  async run(command, args) {
    if (!args) {
      [command, ...args] = command.split(/\s+/);
    }
    return await execa(command, args, { cwd: this.context });
  }

  async resolvePlugins(rawPlugins) {
    // 插件排序，@vue/cli-service 排第1个
    rawPlugins = sortObject(rawPlugins, ["@vue/cli-service"], true);
    const plugins = [];

    for (const id of Object.keys(rawPlugins)) {
      // require('@vue/cli-service/generator')
      // require('@vue/cli-plugin-babel/generator')
      // require('@vue/cli-plugin-eslint/generator')
      const apply = loadModule(`${id}/generator`, this.context) || (() => {});
      let options = rawPlugins[id] || {};
      plugins.push({ id, apply, options });
    }

    return plugins;
  }

  shouldInitGit() {
    if (!hasGit()) {
      // 系统未安装 git
      return false;
    }

    // 项目未初始化 Git
    return !hasProjectGit(this.context);
  }
}

export default Createor;
