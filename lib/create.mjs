import path from "path";
import Createor from "./Createor.mjs";

export async function create(projectName) {
  const cwd = process.cwd();
  const targetDir = path.resolve(cwd, projectName || "");
  const creator = new Createor(projectName, targetDir);
  await creator.create();
}
