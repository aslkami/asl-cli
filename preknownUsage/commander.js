import program from "commander";

program
  .name("cli")
  .description("脚手架demo")
  .version("0.0.2")
  .usage("<command> [options]");

program
  .command("createPage")
  .description("生成页面")
  .argument("<name>", "文件名字")
  .action((name) => {
    console.log("新建一个文件 ", name);
  });

program.parse();
