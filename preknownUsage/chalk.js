import chalk from "chalk";

console.log(
  `${chalk.blue("hello")}, ${chalk.red("this")} ${chalk.underline(
    "is"
  )} ${chalk.bgRed("chalk")}!`
);
