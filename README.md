## cli 创建流程

1. 获取基本配置

   - presetPrompt，基础预设，如选择 vue2、vue3、手动选择
   - featurePrompt，一些想要的特性，如 babel、vuex、pinia、router、typescript 等
   - outroPrompts，外部的东西，如是否将 babel 写入 package.json, 是否保存 预设配置 等
   - injectedPrompts，选择 featurePrompt 额外的配置

2. 在生成的项目目录下，生成 package.json 文件，写入依赖，执行 npm install

3. 初始化 包管理类，用于 判断 npm 版本 初始化仓库、安装包

4. 准备生成文件

   - 解析 @vue/cli 开头的依赖，排序 并 加载对应的 generator 文件
   - 实例化 Generator, 初始化插件，将插件的信息放到 this.pkg 里，然后提取模板内容放到 middleware 里
   - 处理抽离的文件
   - 处理 模板文件， 把抽离的文件，和模板文件内容 都放在 this.files 上 I(middleware 函数在这一步执行)
   - 给 pkg 的 dependencies、devDependencies、scripts 字段排序，写入 files 里，即 file['package,json'] = pkg
   - 将 files 内存里的 文件通通 写入硬盘生成
   - 重新执行 npm install
   - 生成 readme
